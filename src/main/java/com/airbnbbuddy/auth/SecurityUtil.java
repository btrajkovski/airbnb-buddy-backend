package com.airbnbbuddy.auth;

import com.airbnbbuddy.auth.exception.UserNotAuthenticated;
import com.airbnbbuddy.auth.model.MyUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by stefan on 1/21/16.
 */
@Component
public class SecurityUtil {

    public static void logInUser(MyUser user) {
        MyUser userDetails = MyUser.getBuilder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .password(user.getPassword())
                .role(user.getRole())
                .socialSignInProvider(user.getSocialSignInProvider())
                .username(user.getEmail())
                .build();

        Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public static MyUser getUserDetails() throws UserNotAuthenticated {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof MyUser) {
            return (MyUser) principal;
        } else {
            throw new UserNotAuthenticated();
        }
    }

}
