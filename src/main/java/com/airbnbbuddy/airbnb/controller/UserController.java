package com.airbnbbuddy.airbnb.controller;

/**
 * Created by bojan on 7.6.16.
 */

import com.airbnbbuddy.auth.SecurityUtil;
import com.airbnbbuddy.auth.exception.UserNotAuthenticated;
import com.airbnbbuddy.auth.model.MyUser;
import com.airbnbbuddy.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api")
public class UserController {

    private UserRepository userRepository;

    private UsersConnectionRepository usersConnectionRepository;

    @Autowired
    public UserController(UserRepository userRepository, UsersConnectionRepository usersConnectionRepository) {
        this.userRepository = userRepository;
        this.usersConnectionRepository = usersConnectionRepository;
    }


    @RequestMapping("/user")
    public MyUser currentUser() throws UserNotAuthenticated {
        return userRepository.findOne(SecurityUtil.getUserDetails().getId());
    }

    @RequestMapping("/users")
    public List<MyUser> getAllUser() {
        return userRepository.findAll();
    }

}