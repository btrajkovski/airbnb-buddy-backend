package com.airbnbbuddy.airbnb.controller;

import com.airbnbbuddy.airbnb.model.Offer;
import com.airbnbbuddy.airbnb.service.OfferService;
import com.airbnbbuddy.auth.SecurityUtil;
import com.airbnbbuddy.auth.exception.UserNotAuthenticated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by bojan on 14.6.16.
 */
@RestController
@RequestMapping(path = "/api/offers")
public class OfferController {
    private OfferService offerService;

    @Autowired
    public OfferController(OfferService offerService) {
        this.offerService = offerService;
    }

    @Deprecated
    @RequestMapping(method = RequestMethod.GET)
    private List<Offer> getAllOffers() throws UserNotAuthenticated {
        return offerService.getAllOffers(SecurityUtil.getUserDetails());
    }

    @RequestMapping(value = "/{criteriaId}", method = RequestMethod.GET)
    public List<Offer> getOffersForCriteria(@PathVariable("criteriaId") Long criteriaId, @RequestParam(required = false) String sort) throws UserNotAuthenticated {
        List<Offer> offers = offerService.getOffersForCriteria(criteriaId, SecurityUtil.getUserDetails());
        if (offers == null) {
            return new ArrayList<>();
        }
        if (sort == null) sort = "rating";

        if (sort.equals("rating")) {
            List<Offer> sortedOffers = offers.stream()
                    .filter(o -> o.getStar_rating() != null)
                    .sorted((o1, o2) -> Double.compare(o2.getStar_rating(), o1.getStar_rating()))
                    .collect(Collectors.toList());
            sortedOffers.addAll(offers.stream().filter(o -> o.getStar_rating() == null).collect(Collectors.toList()));

            offers = sortedOffers;
        } else if (sort.equals("price")) {
            Collections.sort(offers, (o1, o2) -> Long.compare(o1.getPrice(), o2.getPrice()));
        }

        return offers;
    }
}
