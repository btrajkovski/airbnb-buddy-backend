package com.airbnbbuddy.airbnb.controller.wrapper.api;

import java.util.List;

/**
 * Created by bojan on 7.6.16.
 */
public class AirbnbResponse {
    private List<Result> search_results;
    private Metadata metadata;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<Result> getSearch_results() {
        return search_results;
    }

    public void setSearch_results(List<Result> search_results) {
        this.search_results = search_results;
    }
}
