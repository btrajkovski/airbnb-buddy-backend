package com.airbnbbuddy.airbnb.controller.wrapper.api;

/**
 * Created by bojan on 14.6.16.
 */
public class Metadata {
    private Long listings_count;

    public Long getListings_count() {
        return listings_count;
    }

    public void setListings_count(Long listings_count) {
        this.listings_count = listings_count;
    }
}
