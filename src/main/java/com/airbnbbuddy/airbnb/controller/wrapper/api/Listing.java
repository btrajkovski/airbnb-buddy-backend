package com.airbnbbuddy.airbnb.controller.wrapper.api;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "bedrooms",
        "beds",
        "airbnb_plus_enabled",
        "extra_host_languages",
        "id",
        "instant_bookable",
        "is_business_travel_ready",
        "is_new_listing",
        "lat",
        "lng",
        "name",
        "person_capacity",
        "picture_count",
        "picture_url",
        "picture_urls",
        "property_type",
        "public_address",
        "reviews_count",
        "star_rating",
        "room_type",
        "wishlisted_count",
        "user",
        "primary_host",
        "listing_tags"
})
public class Listing {
    @JsonProperty("bedrooms")
    private Long bedrooms;

    @JsonProperty("beds")
    private Long beds;

    @JsonProperty("airbnb_plus_enabled")
    private Boolean airbnbPlusEnabled;

    @JsonProperty("extra_host_languages")
    private List<String> extraHostLanguages = new ArrayList<String>();

    @JsonProperty("id")
    private Long id;

    @JsonProperty("instant_bookable")
    private Boolean instantBookable;

    @JsonProperty("is_business_travel_ready")
    private Boolean isBusinessTravelReady;

    @JsonProperty("is_new_listing")
    private Boolean isNewListing;

    @JsonProperty("lat")
    private Double lat;

    @JsonProperty("lng")
    private Double lng;

    @JsonProperty("name")
    private String name;

    @JsonProperty("person_capacity")
    private Long personCapacity;

    @JsonProperty("picture_count")
    private Long pictureCount;

    @JsonProperty("picture_url")
    private String pictureUrl;

    @JsonProperty("picture_urls")
    private List<String> pictureUrls = new ArrayList<String>();

    @JsonProperty("property_type")
    private String propertyType;

    @JsonProperty("public_address")
    private String publicAddress;

    @JsonProperty("reviews_count")
    private Long reviewsCount;

    @JsonProperty("star_rating")
    private Float starRating;

    @JsonProperty("room_type")
    private String roomType;

    @JsonProperty("wishlisted_count")
    private Long wishlistedCount;

    @JsonProperty("user")
    private User user;

    @JsonProperty("primary_host")
    private PrimaryHost primaryHost;

    @JsonProperty("listing_tags")
    private List<Object> listingTags = new ArrayList<Object>();

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The bedrooms
     */
    @JsonProperty("bedrooms")
    public Long getBedrooms() {
        return bedrooms;
    }

    /**
     * @param bedrooms The bedrooms
     */
    @JsonProperty("bedrooms")
    public void setBedrooms(Long bedrooms) {
        this.bedrooms = bedrooms;
    }

    /**
     * @return The beds
     */
    @JsonProperty("beds")
    public Long getBeds() {
        return beds;
    }

    /**
     * @param beds The beds
     */
    @JsonProperty("beds")
    public void setBeds(Long beds) {
        this.beds = beds;
    }

    /**
     * @return The airbnbPlusEnabled
     */
    @JsonProperty("airbnb_plus_enabled")
    public Boolean getAirbnbPlusEnabled() {
        return airbnbPlusEnabled;
    }

    /**
     * @param airbnbPlusEnabled The airbnb_plus_enabled
     */
    @JsonProperty("airbnb_plus_enabled")
    public void setAirbnbPlusEnabled(Boolean airbnbPlusEnabled) {
        this.airbnbPlusEnabled = airbnbPlusEnabled;
    }

    /**
     * @return The extraHostLanguages
     */
    @JsonProperty("extra_host_languages")
    public List<String> getExtraHostLanguages() {
        return extraHostLanguages;
    }

    /**
     * @param extraHostLanguages The extra_host_languages
     */
    @JsonProperty("extra_host_languages")
    public void setExtraHostLanguages(List<String> extraHostLanguages) {
        this.extraHostLanguages = extraHostLanguages;
    }

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return The instantBookable
     */
    @JsonProperty("instant_bookable")
    public Boolean getInstantBookable() {
        return instantBookable;
    }

    /**
     * @param instantBookable The instant_bookable
     */
    @JsonProperty("instant_bookable")
    public void setInstantBookable(Boolean instantBookable) {
        this.instantBookable = instantBookable;
    }

    /**
     * @return The isBusinessTravelReady
     */
    @JsonProperty("is_business_travel_ready")
    public Boolean getIsBusinessTravelReady() {
        return isBusinessTravelReady;
    }

    /**
     * @param isBusinessTravelReady The is_business_travel_ready
     */
    @JsonProperty("is_business_travel_ready")
    public void setIsBusinessTravelReady(Boolean isBusinessTravelReady) {
        this.isBusinessTravelReady = isBusinessTravelReady;
    }

    /**
     * @return The isNewListing
     */
    @JsonProperty("is_new_listing")
    public Boolean getIsNewListing() {
        return isNewListing;
    }

    /**
     * @param isNewListing The is_new_listing
     */
    @JsonProperty("is_new_listing")
    public void setIsNewListing(Boolean isNewListing) {
        this.isNewListing = isNewListing;
    }

    /**
     * @return The lat
     */
    @JsonProperty("lat")
    public Double getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    @JsonProperty("lat")
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * @return The lng
     */
    @JsonProperty("lng")
    public Double getLng() {
        return lng;
    }

    /**
     * @param lng The lng
     */
    @JsonProperty("lng")
    public void setLng(Double lng) {
        this.lng = lng;
    }

    /**
     * @return The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The personCapacity
     */
    @JsonProperty("person_capacity")
    public Long getPersonCapacity() {
        return personCapacity;
    }

    /**
     * @param personCapacity The person_capacity
     */
    @JsonProperty("person_capacity")
    public void setPersonCapacity(Long personCapacity) {
        this.personCapacity = personCapacity;
    }

    /**
     * @return The pictureCount
     */
    @JsonProperty("picture_count")
    public Long getPictureCount() {
        return pictureCount;
    }

    /**
     * @param pictureCount The picture_count
     */
    @JsonProperty("picture_count")
    public void setPictureCount(Long pictureCount) {
        this.pictureCount = pictureCount;
    }

    /**
     * @return The pictureUrl
     */
    @JsonProperty("picture_url")
    public String getPictureUrl() {
        return pictureUrl;
    }

    /**
     * @param pictureUrl The picture_url
     */
    @JsonProperty("picture_url")
    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    /**
     * @return The pictureUrls
     */
    @JsonProperty("picture_urls")
    public List<String> getPictureUrls() {
        return pictureUrls;
    }

    /**
     * @param pictureUrls The picture_urls
     */
    @JsonProperty("picture_urls")
    public void setPictureUrls(List<String> pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    /**
     * @return The propertyType
     */
    @JsonProperty("property_type")
    public String getPropertyType() {
        return propertyType;
    }

    /**
     * @param propertyType The property_type
     */
    @JsonProperty("property_type")
    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    /**
     * @return The publicAddress
     */
    @JsonProperty("public_address")
    public String getPublicAddress() {
        return publicAddress;
    }

    /**
     * @param publicAddress The public_address
     */
    @JsonProperty("public_address")
    public void setPublicAddress(String publicAddress) {
        this.publicAddress = publicAddress;
    }

    /**
     * @return The reviewsCount
     */
    @JsonProperty("reviews_count")
    public Long getReviewsCount() {
        return reviewsCount;
    }

    /**
     * @param reviewsCount The reviews_count
     */
    @JsonProperty("reviews_count")
    public void setReviewsCount(Long reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    /**
     * @return The starRating
     */
    @JsonProperty("star_rating")
    public Float getStarRating() {
        return starRating;
    }

    /**
     * @param starRating The star_rating
     */
    @JsonProperty("star_rating")
    public void setStarRating(Float starRating) {
        this.starRating = starRating;
    }

    /**
     * @return The roomType
     */
    @JsonProperty("room_type")
    public String getRoomType() {
        return roomType;
    }

    /**
     * @param roomType The room_type
     */
    @JsonProperty("room_type")
    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    /**
     * @return The wishlistedCount
     */
    @JsonProperty("wishlisted_count")
    public Long getWishlistedCount() {
        return wishlistedCount;
    }

    /**
     * @param wishlistedCount The wishlisted_count
     */
    @JsonProperty("wishlisted_count")
    public void setWishlistedCount(Long wishlistedCount) {
        this.wishlistedCount = wishlistedCount;
    }

    /**
     * @return The user
     */
    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    @JsonProperty("user")
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return The primaryHost
     */
    @JsonProperty("primary_host")
    public PrimaryHost getPrimaryHost() {
        return primaryHost;
    }

    /**
     * @param primaryHost The primary_host
     */
    @JsonProperty("primary_host")
    public void setPrimaryHost(PrimaryHost primaryHost) {
        this.primaryHost = primaryHost;
    }

    /**
     * @return The listingTags
     */
    @JsonProperty("listing_tags")
    public List<Object> getListingTags() {
        return listingTags;
    }

    /**
     * @param listingTags The listing_tags
     */
    @JsonProperty("listing_tags")
    public void setListingTags(List<Object> listingTags) {
        this.listingTags = listingTags;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
