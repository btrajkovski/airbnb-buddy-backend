package com.airbnbbuddy.airbnb.controller.wrapper.api;

import com.airbnbbuddy.airbnb.model.Offer;

/**
 * Created by bojan on 7.6.16.
 */
public class Result {
    private Listing listing;
    private PricingQuote pricing_quote;

    public Listing getListing() {
        return listing;
    }

    public void setListing(Listing listing) {
        this.listing = listing;
    }

    public PricingQuote getPricing_quote() {
        return pricing_quote;
    }

    public void setPricing_quote(PricingQuote pricing_quote) {
        this.pricing_quote = pricing_quote;
    }

    public Offer toOffer() {
        Rate rate = getPricing_quote().getRate();
        Offer offer = new Offer(listing.getBedrooms(),
                listing.getId(),
                listing.getPersonCapacity(),
                listing.getPictureUrl(),
                listing.getName(),
                listing.getBeds(),
                rate.getAmount(),
                rate.getCurrency(),
                listing.getStarRating(),
                listing.getReviewsCount());

        return offer;
    }
}
