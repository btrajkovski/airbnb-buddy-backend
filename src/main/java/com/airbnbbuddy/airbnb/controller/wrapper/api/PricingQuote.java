package com.airbnbbuddy.airbnb.controller.wrapper.api;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "can_instant_book",
        "check_in",
        "check_out",
        "guests",
        "rate",
        "rate_type",
        "is_good_price",
        "average_booked_price"
})
public class PricingQuote {

    @JsonProperty("can_instant_book")
    private Boolean canInstantBook;

    @JsonProperty("check_in")
    private String checkIn;

    @JsonProperty("check_out")
    private String checkOut;

    @JsonProperty("guests")
    private Integer guests;

    @JsonProperty("rate")
    private Rate rate;

    @JsonProperty("rate_type")
    private String rateType;

    @JsonProperty("is_good_price")
    private Object isGoodPrice;

    @JsonProperty("average_booked_price")
    private Object averageBookedPrice;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The canInstantBook
     */
    @JsonProperty("can_instant_book")
    public Boolean getCanInstantBook() {
        return canInstantBook;
    }

    /**
     * @param canInstantBook The can_instant_book
     */
    @JsonProperty("can_instant_book")
    public void setCanInstantBook(Boolean canInstantBook) {
        this.canInstantBook = canInstantBook;
    }

    /**
     * @return The checkIn
     */
    @JsonProperty("check_in")
    public String getCheckIn() {
        return checkIn;
    }

    /**
     * @param checkIn The check_in
     */
    @JsonProperty("check_in")
    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    /**
     * @return The checkOut
     */
    @JsonProperty("check_out")
    public String getCheckOut() {
        return checkOut;
    }

    /**
     * @param checkOut The check_out
     */
    @JsonProperty("check_out")
    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    /**
     * @return The guests
     */
    @JsonProperty("guests")
    public Integer getGuests() {
        return guests;
    }

    /**
     * @param guests The guests
     */
    @JsonProperty("guests")
    public void setGuests(Integer guests) {
        this.guests = guests;
    }

    /**
     * @return The rate
     */
    @JsonProperty("rate")
    public Rate getRate() {
        return rate;
    }

    /**
     * @param rate The rate
     */
    @JsonProperty("rate")
    public void setRate(Rate rate) {
        this.rate = rate;
    }

    /**
     * @return The rateType
     */
    @JsonProperty("rate_type")
    public String getRateType() {
        return rateType;
    }

    /**
     * @param rateType The rate_type
     */
    @JsonProperty("rate_type")
    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    /**
     * @return The isGoodPrice
     */
    @JsonProperty("is_good_price")
    public Object getIsGoodPrice() {
        return isGoodPrice;
    }

    /**
     * @param isGoodPrice The is_good_price
     */
    @JsonProperty("is_good_price")
    public void setIsGoodPrice(Object isGoodPrice) {
        this.isGoodPrice = isGoodPrice;
    }

    /**
     * @return The averageBookedPrice
     */
    @JsonProperty("average_booked_price")
    public Object getAverageBookedPrice() {
        return averageBookedPrice;
    }

    /**
     * @param averageBookedPrice The average_booked_price
     */
    @JsonProperty("average_booked_price")
    public void setAverageBookedPrice(Object averageBookedPrice) {
        this.averageBookedPrice = averageBookedPrice;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
