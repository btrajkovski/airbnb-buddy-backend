package com.airbnbbuddy.airbnb.controller;

import com.airbnbbuddy.airbnb.model.UserInfo;
import com.airbnbbuddy.airbnb.service.UserInfoService;
import com.airbnbbuddy.auth.SecurityUtil;
import com.airbnbbuddy.auth.exception.UserNotAuthenticated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by stefan on 6/19/16.
 */
@RestController
@RequestMapping("/api/user-info")
public class UserInfoController {

    private UserInfoService userInfoService;

    @Autowired
    public UserInfoController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    @RequestMapping
    public UserInfo getUserInfo() throws UserNotAuthenticated {
        return userInfoService.findByUser(SecurityUtil.getUserDetails());
    }

    @RequestMapping(method = RequestMethod.POST)
    public UserInfo updateUserInfo(@RequestBody UserInfo userInfo) throws UserNotAuthenticated {
        return userInfoService.addOrUpdate(SecurityUtil.getUserDetails(), userInfo);
    }

}
