package com.airbnbbuddy.airbnb.controller;

import com.airbnbbuddy.airbnb.model.Criteria;
import com.airbnbbuddy.airbnb.model.Parameter;
import com.airbnbbuddy.airbnb.model.base.BaseEntity;
import com.airbnbbuddy.airbnb.service.CriteriaService;
import com.airbnbbuddy.airbnb.service.OfferService;
import com.airbnbbuddy.auth.SecurityUtil;
import com.airbnbbuddy.auth.exception.UserNotAuthenticated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by bojan on 9.6.16.
 */
@RestController
@RequestMapping(path = "/api/criteria")
public class CriteriaController {
    private CriteriaService criteriaService;

    @Autowired
    public CriteriaController(CriteriaService criteriaService) {
        this.criteriaService = criteriaService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public BaseEntity addCriteria(@RequestBody List<Parameter> parameters) throws UserNotAuthenticated {
        return criteriaService.save(new Criteria(parameters));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List listCriteria() throws UserNotAuthenticated {
        return criteriaService.findCriteriaByUser(SecurityUtil.getUserDetails());
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCriteria(@PathVariable Long id) {
        criteriaService.loadAndDelete(id);
    }
}