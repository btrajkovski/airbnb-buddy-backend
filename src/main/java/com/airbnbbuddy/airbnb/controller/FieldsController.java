package com.airbnbbuddy.airbnb.controller;

import com.airbnbbuddy.airbnb.model.Field;
import com.airbnbbuddy.airbnb.service.FieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by bojan on 7.6.16.
 */
@RestController
@RequestMapping(value = "/api")
public class FieldsController {
    private FieldService fieldService;

    @Autowired
    public FieldsController(FieldService fieldService) {
        this.fieldService = fieldService;
    }

    @RequestMapping(value = "/fields")
    public List<Field> listAllFields() {
        return fieldService.findAll();
    }
}
