package com.airbnbbuddy.airbnb.service;

import com.airbnbbuddy.airbnb.model.Criteria;
import com.airbnbbuddy.airbnb.model.Offer;
import com.airbnbbuddy.auth.exception.UserNotAuthenticated;
import com.airbnbbuddy.auth.model.MyUser;

import java.util.List;

/**
 * Created by bojan on 11.6.16.
 */
public interface OfferService extends BaseService {
    void generateOffers(Long criteriaId, MyUser currentUser) throws UserNotAuthenticated;

    void makeSingleRequest(MyUser currentUser, Criteria criteria, int offset);

    List<Offer> getAllOffers(MyUser currentUser) throws UserNotAuthenticated;

    List<Offer> getOffersForCriteria(Long criteriaId, MyUser currentUser) throws UserNotAuthenticated;
}
