package com.airbnbbuddy.airbnb.service.utils;

import com.airbnbbuddy.airbnb.model.Criteria;
import org.springframework.stereotype.Component;

/**
 * Created by bojan on 11.6.16.
 */
@Component
public class AirbnbAPI {
    private static String baseApiUrl = "https://api.airbnb.com/v2/search_results?client_id=3092nxybyb0otqw18e8nh5nty";
    private static Integer maxLimit = 50;

    public static String getBaseApiUrl() {
        return baseApiUrl;
    }

    public static Integer getMaxLimit() {
        return maxLimit;
    }

    public static void setBaseApiUrl(String baseApiUrl) {
        AirbnbAPI.baseApiUrl = baseApiUrl;
    }

    public static String getAirbnbRequestUrl(Criteria criteria, Integer offset) {
        return getAirbnbRequestUrl(criteria, offset, maxLimit);
    }

    public static String getAirbnbRequestUrl(Criteria criteria, Integer offset, Integer limit) {
        return String.format("%s" +
                        "%s" +
                        "&_offset=%d" +
                        "&_limit=%d" +
                        "&_format=for_search_results_with_minimal_pricing" +
                        "&currency=USD" +
                        "&sort=1",
                baseApiUrl, criteria.toQuery(), offset, limit);
    }
}
