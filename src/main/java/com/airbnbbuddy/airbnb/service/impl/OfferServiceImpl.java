package com.airbnbbuddy.airbnb.service.impl;

import com.airbnbbuddy.airbnb.controller.wrapper.api.AirbnbResponse;
import com.airbnbbuddy.airbnb.controller.wrapper.api.Result;
import com.airbnbbuddy.airbnb.model.Criteria;
import com.airbnbbuddy.airbnb.model.Offer;
import com.airbnbbuddy.airbnb.repository.CriteriaRepository;
import com.airbnbbuddy.airbnb.repository.OfferRepository;
import com.airbnbbuddy.airbnb.service.OfferService;
import com.airbnbbuddy.airbnb.service.utils.AirbnbAPI;
import com.airbnbbuddy.auth.exception.UserNotAuthenticated;
import com.airbnbbuddy.auth.model.MyUser;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Objects;

/**
 * Created by bojan on 11.6.16.
 */
@Service
public class OfferServiceImpl extends BaseServiceImpl implements OfferService {
    private static final Logger logger = LoggerFactory.getLogger(OfferService.class);
    private OfferRepository offerRepository;
    private RestTemplate restTemplate;
    private CriteriaRepository criteriaRepository;


    @Autowired
    public OfferServiceImpl(OfferRepository offerRepository, CriteriaRepository criteriaRepository) {
        this.offerRepository = offerRepository;
        this.criteriaRepository = criteriaRepository;
        restTemplate = new RestTemplate();
    }

    @Override
    @PostConstruct
    public void init() {
        super.setRepository(offerRepository);
    }

    @Override
    @Async
    public void generateOffers(Long criteriaId, MyUser currentUser) throws UserNotAuthenticated {
        Criteria criteria = criteriaRepository.findOne(criteriaId);
        AirbnbResponse response = restTemplate.getForObject(AirbnbAPI.getAirbnbRequestUrl(criteria, 0, 0), AirbnbResponse.class);
        int numRequests = (int) Math.ceil(response.getMetadata().getListings_count() / 50.0);
        // Limit num offer per criteria to 150
        numRequests = Math.min(numRequests, 3);
        int offset = 0;

        DateTime startDateTime = new DateTime();

        for (int i = 0; i < numRequests; i++) {
            makeSingleRequest(currentUser, criteria, offset);
            offset += AirbnbAPI.getMaxLimit();
        }

        List<Offer> oldOffers = offerRepository.findByCriteriaAndLastModifiedDateBefore(criteria, startDateTime);
        if (oldOffers.size() > 0) {
            logger.info(String.format("Deleting %d old offers, no longer available", oldOffers.size()));
            offerRepository.delete(oldOffers);
        }
    }

    @Override
    @Async
    public void makeSingleRequest(MyUser currentUser, Criteria criteria, int offset) {
        AirbnbResponse response;
        response = restTemplate.getForObject(AirbnbAPI.getAirbnbRequestUrl(criteria, offset), AirbnbResponse.class);
        logger.info(AirbnbAPI.getAirbnbRequestUrl(criteria, offset));

        for (Result result :
                response.getSearch_results()) {
            Offer offer = result.toOffer();
            Offer oldOffer = offerRepository.findByAirbnbIdAndUser(offer.getAirbnbId(), currentUser);
            if (oldOffer == null) {
                offer.setUpdatedPrice(true);
            } else {
                offer.setId(oldOffer.getId());
                offer.setCreatedDate(oldOffer.getCreatedDate());
                offer.setUpdatedPrice(oldOffer.getPrice().longValue() != offer.getPrice().longValue());
            }
            offer.setUser(currentUser);
            offer.setCriteria(criteria);
            offerRepository.save(offer);
        }
    }

    @Override
    public List<Offer> getAllOffers(MyUser currentUser) throws UserNotAuthenticated {
        return offerRepository.findByUser(currentUser);
    }

    @Override
    public List<Offer> getOffersForCriteria(Long criteriaId, MyUser currentUser) {
        Criteria criteria = criteriaRepository.findOne(criteriaId);
        if (!Objects.equals(criteria.getUser().getId(), currentUser.getId())) {
            return null;
        }

        return offerRepository.findByCriteriaAndUser(criteria, currentUser);
    }
}
