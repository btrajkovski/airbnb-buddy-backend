package com.airbnbbuddy.airbnb.service.impl;

import com.airbnbbuddy.airbnb.repository.FieldRepository;
import com.airbnbbuddy.airbnb.service.FieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by bojan on 7.6.16.
 */
@Service
public class FieldServiceImpl extends BaseServiceImpl implements FieldService {
    private FieldRepository fieldRepository;

    @Autowired
    public FieldServiceImpl(FieldRepository fieldRepository) {
        this.fieldRepository = fieldRepository;
    }

    @Override
    @PostConstruct
    public void init() {
        super.setRepository(fieldRepository);
    }
}
