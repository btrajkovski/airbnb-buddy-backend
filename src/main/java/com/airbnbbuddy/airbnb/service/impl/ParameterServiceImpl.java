package com.airbnbbuddy.airbnb.service.impl;

import com.airbnbbuddy.airbnb.repository.ParameterRepository;
import com.airbnbbuddy.airbnb.service.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by bojan on 7.6.16.
 */
@Service
public class ParameterServiceImpl extends BaseServiceImpl implements ParameterService {
    private ParameterRepository parameterRepository;

    @Autowired
    public ParameterServiceImpl(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    @Override
    @PostConstruct
    public void init() {
        super.setRepository(parameterRepository);
    }
}
