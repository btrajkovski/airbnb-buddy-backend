package com.airbnbbuddy.airbnb.service.impl;

import com.airbnbbuddy.airbnb.model.Criteria;
import com.airbnbbuddy.airbnb.model.base.BaseEntity;
import com.airbnbbuddy.airbnb.repository.CriteriaRepository;
import com.airbnbbuddy.airbnb.service.CriteriaService;
import com.airbnbbuddy.airbnb.service.OfferService;
import com.airbnbbuddy.auth.SecurityUtil;
import com.airbnbbuddy.auth.exception.UserNotAuthenticated;
import com.airbnbbuddy.auth.model.MyUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by bojan on 7.6.16.
 */
@Service
public class CriteriaServiceImpl extends BaseServiceImpl implements CriteriaService {
    private CriteriaRepository criteriaRepository;
    private OfferService offerService;

    @Autowired
    public CriteriaServiceImpl(CriteriaRepository criteriaRepository, OfferService offerService) {
        this.criteriaRepository = criteriaRepository;
        this.offerService = offerService;
    }

    @Override
    @PostConstruct
    public void init() {
        super.setRepository(criteriaRepository);
    }

    @Override
    public BaseEntity save(BaseEntity entity) throws UserNotAuthenticated {
        Criteria criteria = (Criteria) entity;
        criteria.setUser(SecurityUtil.getUserDetails());
        criteria = (Criteria) super.save(criteria);
        offerService.generateOffers(criteria.getId(), SecurityUtil.getUserDetails());
        return criteria;
    }

    @Override
    public List<Criteria> findCriteriaByUser(MyUser user) {
        return criteriaRepository.findByUser(user);
    }
}
