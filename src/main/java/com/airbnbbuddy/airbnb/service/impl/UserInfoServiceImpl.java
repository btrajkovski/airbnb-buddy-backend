package com.airbnbbuddy.airbnb.service.impl;

import com.airbnbbuddy.airbnb.model.UserInfo;
import com.airbnbbuddy.airbnb.repository.UserInfoRepository;
import com.airbnbbuddy.airbnb.service.UserInfoService;
import com.airbnbbuddy.auth.model.MyUser;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by stefan on 6/19/16.
 */
@Service
public class UserInfoServiceImpl extends BaseServiceImpl implements UserInfoService {

    private UserInfoRepository userInfoRepository;

    @Autowired
    public UserInfoServiceImpl(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }

    @Override
    public void init() {
        super.setRepository(userInfoRepository);
    }

    @Override
    public UserInfo findByUser(MyUser user) {
        return userInfoRepository.findByUser(user);
    }

    @Override
    public UserInfo addOrUpdate(MyUser user, UserInfo userInfo) {
        UserInfo inDb = userInfoRepository.findByUser(user);
        if (inDb == null) {
            inDb = new UserInfo();
        }
        inDb.setUser(user);
        inDb.setEmail(userInfo.getEmail());
        inDb.setNotificationFrequency(userInfo.getNotificationFrequency());
        if (inDb.getLastNotified() == null) {
            inDb.setLastNotified(new DateTime(0L));
        }

        return userInfoRepository.save(inDb);
    }
}
