package com.airbnbbuddy.airbnb.service.impl;

import com.airbnbbuddy.airbnb.model.UserInfo;
import com.airbnbbuddy.airbnb.service.MailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.util.Locale;

/**
 * Created by bojan on 19.6.16.
 */
@Service
public class MailSenderImpl implements MailSender {
    private JavaMailSender javaMailSender;
    private SpringTemplateEngine templateEngine;
    private static final Logger logger = LoggerFactory.getLogger(MailSenderImpl.class);

    @Value("${app.url}")
    private String baseUrl;

    @Autowired
    public MailSenderImpl(JavaMailSender javaMailSender, SpringTemplateEngine templateEngine) {
        this.javaMailSender = javaMailSender;
        this.templateEngine = templateEngine;
    }

    @Override
    @Async
    public void sendEmail(UserInfo user, String subject, String content, boolean isHtml) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message =
                    new MimeMessageHelper(
                            mimeMessage, false, "UTF-8");
            message.setTo(user.getEmail());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);

        } catch (Exception e) {
            logger.error(String.format("Error while sending email to %s, message %s", user.getEmail(), e.getMessage()));
        }
        javaMailSender.send(mimeMessage);
    }

    @Override
    public void sendUserNotificationEmail(UserInfo user, Long newOffers) {
        Locale locale = Locale.getDefault();
        Context context = new Context(locale);
        context.setVariable("user", user.getUser());
        context.setVariable("baseUrl", baseUrl);
        context.setVariable("newOffers", newOffers);
        String content = templateEngine.process("mail/offersEmail", context);
        this.sendEmail(user, "New Offers Available", content, true);
    }
}
