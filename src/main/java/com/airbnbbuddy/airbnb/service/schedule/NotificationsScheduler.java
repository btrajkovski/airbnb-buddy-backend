package com.airbnbbuddy.airbnb.service.schedule;

import com.airbnbbuddy.airbnb.model.Criteria;
import com.airbnbbuddy.airbnb.model.UserInfo;
import com.airbnbbuddy.airbnb.model.enums.NotificationFrequency;
import com.airbnbbuddy.airbnb.repository.OfferRepository;
import com.airbnbbuddy.airbnb.repository.UserInfoRepository;
import com.airbnbbuddy.airbnb.service.CriteriaService;
import com.airbnbbuddy.airbnb.service.MailSender;
import com.airbnbbuddy.airbnb.service.OfferService;
import com.airbnbbuddy.auth.exception.UserNotAuthenticated;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by bojan on 14.6.16.
 */
@Service
public class NotificationsScheduler {
    private Logger logger = LoggerFactory.getLogger(NotificationsScheduler.class);
    private OfferService offerService;
    private CriteriaService criteriaService;
    private OfferRepository offerRepository;
    private UserInfoRepository userInfoRepository;
    private MailSender mailSender;

    @Autowired
    public NotificationsScheduler(OfferService offerService, CriteriaService criteriaService, OfferRepository offerRepository, UserInfoRepository userInfoRepository, MailSender mailSender) {
        this.offerService = offerService;
        this.criteriaService = criteriaService;
        this.offerRepository = offerRepository;
        this.userInfoRepository = userInfoRepository;
        this.mailSender = mailSender;
    }

    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void getNewOffers() throws UserNotAuthenticated {
        List<UserInfo> usersToCheck = getUsersToCheck();

        for (UserInfo userInfo :
                usersToCheck) {
            List<Criteria> criterias = criteriaService.findCriteriaByUser(userInfo.getUser());

            logger.info(String.format("Generating new offers for User %s", userInfo.getEmail()));
            for (Criteria criteria :
                    criterias) {
                offerService.generateOffers(criteria.getId(), userInfo.getUser());
            }
        }
    }

    @Scheduled(fixedRate = 60 * 60 * 1000, initialDelay = 30 * 60 * 1000)
    public void notifyUsers() throws UserNotAuthenticated {
        List<UserInfo> usersToCheck = getUsersToCheck();

        for (UserInfo userInfo :
                usersToCheck) {

            List<Criteria> criterias = criteriaService.findCriteriaByUser(userInfo.getUser());
            Long newOffers = 0L;

            for (Criteria criteria :
                    criterias) {
                newOffers += offerRepository.countByCriteriaAndUpdatedPrice(criteria, true);
            }

            logger.info(String.format("Found %d new offers for User %s", newOffers, userInfo.getEmail()));
            if (newOffers > 0) {
                mailSender.sendUserNotificationEmail(userInfo, newOffers);
            }

            userInfo.setLastNotified(new DateTime());
            userInfoRepository.save(userInfo);
        }
    }

    private List<UserInfo> getUsersToCheck() {
        DateTime currentDateTime = new DateTime();
        List<UserInfo> usersToCheck = userInfoRepository.findByNotificationFrequencyAndLastNotifiedBefore(
                NotificationFrequency.HOURLY, currentDateTime.minusHours(1));
        usersToCheck.addAll(userInfoRepository.findByNotificationFrequencyAndLastNotifiedBefore(
                NotificationFrequency.DAILY, currentDateTime.minusDays(1)
        ));
        return usersToCheck;
    }
}
