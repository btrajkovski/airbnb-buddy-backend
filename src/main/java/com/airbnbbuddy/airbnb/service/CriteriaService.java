package com.airbnbbuddy.airbnb.service;

import com.airbnbbuddy.airbnb.model.Criteria;
import com.airbnbbuddy.auth.model.MyUser;

import java.util.List;

/**
 * Created by bojan on 9.6.16.
 */
public interface CriteriaService extends BaseService {
    List<Criteria> findCriteriaByUser(MyUser user);
}
