package com.airbnbbuddy.airbnb.service;

import com.airbnbbuddy.airbnb.model.UserInfo;

/**
 * Created by bojan on 19.6.16.
 */
public interface MailSender {
    void sendEmail(UserInfo user, String subject, String content, boolean isHtml);

    void sendUserNotificationEmail(UserInfo user, Long newOffers);
}