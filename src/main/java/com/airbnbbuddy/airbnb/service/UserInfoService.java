package com.airbnbbuddy.airbnb.service;

import com.airbnbbuddy.airbnb.model.UserInfo;
import com.airbnbbuddy.auth.model.MyUser;

/**
 * Created by stefan on 6/19/16.
 */
public interface UserInfoService extends BaseService {
    UserInfo findByUser(MyUser user);

    UserInfo addOrUpdate(MyUser user, UserInfo userInfo);
}
