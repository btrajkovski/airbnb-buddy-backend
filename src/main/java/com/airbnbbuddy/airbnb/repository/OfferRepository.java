package com.airbnbbuddy.airbnb.repository;

import com.airbnbbuddy.airbnb.model.Criteria;
import com.airbnbbuddy.airbnb.model.Offer;
import com.airbnbbuddy.auth.model.MyUser;
import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by bojan on 11.6.16.
 */
public interface OfferRepository extends JpaRepository<Offer, Long> {
    Offer findByAirbnbIdAndUser(Long airbnbId, MyUser user);

    List<Offer> findByCriteriaAndUser(Criteria criteria, MyUser user);

    List<Offer> findByUser(MyUser currentUser);

    List<Offer> findByCriteriaAndLastModifiedDateAfter(Criteria criteria, DateTime lastModifiedDate);

    List<Offer> findByCriteriaAndLastModifiedDateBefore(Criteria criteria, DateTime lastModifiedDate);

    Long countByCriteriaAndUpdatedPrice(Criteria criteria, boolean updatedPrice);
}
