package com.airbnbbuddy.airbnb.repository;

import com.airbnbbuddy.airbnb.model.Criteria;
import com.airbnbbuddy.auth.model.MyUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by bojan on 9.6.16.
 */
public interface CriteriaRepository extends JpaRepository<Criteria, Long> {
    List<Criteria> findByUser(MyUser user);
}
