package com.airbnbbuddy.airbnb.repository;

import com.airbnbbuddy.airbnb.model.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by bojan on 9.6.16.
 */
public interface ParameterRepository extends JpaRepository<Parameter, Long> {
}
