package com.airbnbbuddy.airbnb.repository;

import com.airbnbbuddy.airbnb.model.UserInfo;
import com.airbnbbuddy.airbnb.model.enums.NotificationFrequency;
import com.airbnbbuddy.auth.model.MyUser;
import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by stefan on 6/19/16.
 */
public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
    UserInfo findByUser(MyUser user);

    List<UserInfo> findByNotificationFrequencyAndLastNotifiedBefore(NotificationFrequency notificationFrequency, DateTime lastNotifiedBefore);
}