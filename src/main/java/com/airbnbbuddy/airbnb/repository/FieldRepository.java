package com.airbnbbuddy.airbnb.repository;

import com.airbnbbuddy.airbnb.model.Field;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by bojan on 7.6.16.
 */
public interface FieldRepository extends JpaRepository<Field, Long> {
}