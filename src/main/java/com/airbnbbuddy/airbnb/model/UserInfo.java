package com.airbnbbuddy.airbnb.model;

import com.airbnbbuddy.airbnb.model.base.BaseEntity;
import com.airbnbbuddy.airbnb.model.enums.NotificationFrequency;
import com.airbnbbuddy.auth.model.MyUser;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

/**
 * Created by stefan on 6/19/16.
 */
@Entity
public class UserInfo extends BaseEntity<Long> {

    @OneToOne
    private MyUser user;

    private String email;

    @Enumerated(EnumType.STRING)
    private NotificationFrequency notificationFrequency;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime lastNotified;

    public UserInfo() {
    }

    public MyUser getUser() {
        return user;
    }

    public void setUser(MyUser user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public NotificationFrequency getNotificationFrequency() {
        return notificationFrequency;
    }

    public void setNotificationFrequency(NotificationFrequency notificationFrequency) {
        this.notificationFrequency = notificationFrequency;
    }

    public DateTime getLastNotified() {
        return lastNotified;
    }

    public void setLastNotified(DateTime lastNotified) {
        this.lastNotified = lastNotified;
    }
}
