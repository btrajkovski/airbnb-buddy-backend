package com.airbnbbuddy.airbnb.model.enums;

/**
 * Created by stefan on 6/19/16.
 */
public enum NotificationFrequency {
    HOURLY,
    DAILY
}
