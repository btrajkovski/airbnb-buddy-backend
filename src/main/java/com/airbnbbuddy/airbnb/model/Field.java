package com.airbnbbuddy.airbnb.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bojan on 7.6.16.
 */
@Entity
public class Field {
    @Column(name = "field_name")
    private String name;

    @Column(name = "field_type")
    private String type;

    @Column(name = "field_from")
    private String from;

    @Column(name = "field_to")
    private String to;

    @Column(name = "field_step")
    private String step;

    @Column(name = "field_enums")
    @ElementCollection
    private List<String> enums = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Field() {
    }

    public Field(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public Field(String name, String type, List<String> enums) {
        this.name = name;
        this.type = type;
        this.enums = enums;
    }

    public Field(String name, String type, String from, String to, String step) {
        this.name = name;
        this.type = type;
        this.from = from;
        this.to = to;
        this.step = step;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public List<String> getEnums() {
        return enums;
    }

    public void setEnums(List<String> enums) {
        this.enums = enums;
    }
}
