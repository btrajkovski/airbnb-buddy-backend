package com.airbnbbuddy.airbnb.model;

import com.airbnbbuddy.airbnb.model.base.BaseEntity;
import com.airbnbbuddy.auth.model.MyUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Created by bojan on 8.6.16.
 */
@Entity
public class Criteria extends BaseEntity<Long> {

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Parameter> parameters;

    @OneToOne
    private MyUser user;

    @OneToMany(mappedBy = "criteria", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Offer> offers;

    @Transient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer offersCount;

    public Criteria() {
    }

    public Criteria(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    @JsonIgnore
    public MyUser getUser() {
        return user;
    }

    public void setUser(MyUser user) {
        this.user = user;
    }

    public String toQuery() {
        StringBuilder query = new StringBuilder();
        for (Parameter parameter :
                parameters) {
            try {
                query.append(String.format("&%s=%s", parameter.getName(),
                        URLEncoder.encode(parameter.getValue(), StandardCharsets.UTF_8.toString())));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        return query.toString();
    }

    public Integer getOffersCount() {
        return offers == null ? 0 : offers.size();
    }
}
