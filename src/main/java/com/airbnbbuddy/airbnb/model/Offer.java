package com.airbnbbuddy.airbnb.model;

import com.airbnbbuddy.airbnb.model.base.BaseEntity;
import com.airbnbbuddy.auth.model.MyUser;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 * Created by bojan on 11.6.16.
 */
@Entity
public class Offer extends BaseEntity<Long> {

    private Long bedrooms;

    private Long airbnbId;

    private Long person_capacity;

    private String picture_url;

    private String name;

    private Long beds;

    private Long price;

    private String priceCurrency;

    private Float star_rating;

    private Long reviews_count;

    private boolean updatedPrice;

    @JsonIgnore
    @OneToOne
    private MyUser user;

    @JsonIgnore
    @ManyToOne
    private Criteria criteria;

    @Transient
    private Long criteriaID;

    @Transient
    private String location;

    public Offer() {
    }

    public Offer(Long bedrooms, Long airbnbId, Long person_capacity, String picture_url, String name, Long beds, Long price, String priceCurrency, Float star_rating, Long reviews_count) {
        this.bedrooms = bedrooms;
        this.airbnbId = airbnbId;
        this.person_capacity = person_capacity;
        this.picture_url = picture_url;
        this.price = price;
        this.priceCurrency = priceCurrency;
        this.name = name;
        this.beds = beds;
        this.star_rating = star_rating;
        this.reviews_count = reviews_count;
    }

    public Long getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(Long bedrooms) {
        this.bedrooms = bedrooms;
    }

    public Long getAirbnbId() {
        return airbnbId;
    }

    public void setAirbnbId(Long airbnbId) {
        this.airbnbId = airbnbId;
    }

    public Long getPerson_capacity() {
        return person_capacity;
    }

    public void setPerson_capacity(Long person_capacity) {
        this.person_capacity = person_capacity;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getPriceCurrency() {
        return priceCurrency;
    }

    public void setPriceCurrency(String priceCurrency) {
        this.priceCurrency = priceCurrency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBeds() {
        return beds;
    }

    public void setBeds(Long beds) {
        this.beds = beds;
    }


    public MyUser getUser() {
        return user;
    }

    public void setUser(MyUser user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Offer) {
            return airbnbId.equals(((Offer) obj).getAirbnbId());
        }
        return false;
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }

    public Long getCriteriaID() {
        return criteria.getId();
    }

    public String getLocation() {
        for (Parameter p :
                criteria.getParameters()) {

            if (p.getName().equals("location")) {
                return p.getValue();
            }
        }
        return "";
    }

    public Float getStar_rating() {
        return star_rating;
    }

    public void setStar_rating(Float star_rating) {
        this.star_rating = star_rating;
    }

    public Long getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(Long reviews_count) {
        this.reviews_count = reviews_count;
    }

    public boolean getIsUpdatedPrice() {
        return updatedPrice;
    }

    public void setUpdatedPrice(boolean updatedPrice) {
        this.updatedPrice = updatedPrice;
    }
}
