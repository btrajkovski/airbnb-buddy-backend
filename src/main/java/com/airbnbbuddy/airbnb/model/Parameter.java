package com.airbnbbuddy.airbnb.model;

import com.airbnbbuddy.airbnb.model.base.BaseEntity;

import javax.persistence.Entity;

/**
 * Created by bojan on 8.6.16.
 */
@Entity
public class Parameter extends BaseEntity<Long> {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
