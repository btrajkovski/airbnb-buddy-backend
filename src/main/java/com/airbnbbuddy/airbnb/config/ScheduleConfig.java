package com.airbnbbuddy.airbnb.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by bojan on 14.6.16.
 */
@Configuration
@EnableScheduling
@EnableAsync
public class ScheduleConfig {
}
