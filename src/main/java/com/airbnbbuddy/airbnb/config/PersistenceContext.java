package com.airbnbbuddy.airbnb.config;

import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by stefan on 1/14/16.
 */
@Configuration
@EnableJpaRepositories(basePackages = {
        "com.airbnbbuddy.auth",
        "com.airbnbbuddy.airbnb"
})
@EntityScan(basePackages = {
        "com.airbnbbuddy.auth.model",
        "com.airbnbbuddy.airbnb.model"
})
@EnableJpaAuditing
public class PersistenceContext {

}
