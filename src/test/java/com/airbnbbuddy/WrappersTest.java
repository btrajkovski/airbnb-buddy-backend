package com.airbnbbuddy;

import com.airbnbbuddy.airbnb.controller.wrapper.api.AirbnbResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

/**
 * Created by bojan on 7.6.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AirbnbBuddyApplication.class)
@WebIntegrationTest
public class WrappersTest {
    RestTemplate restTemplate = new RestTemplate();
    private String testAirbnbUrl = "https://api.airbnb.com/v2/search_results?client_id=3092nxybyb0otqw18e8nh5nty&locale=en-US&currency=USD&_format=for_search_results_with_minimal_pricing&_limit=50&_offset=0&fetch_facets=true&guests=1&ib=false&ib_add_photo_flow=true&location=Paris,%20France&checkin=2016-09-01&checkout=2016-09-12&min_bathrooms=0&min_bedrooms=0&min_beds=1&min_num_pic_urls=10&price_max=500&sort=1&user_lat=37.3398634&user_lng=-122.0455164";

    @Test
    public void testAirbnbWrapper() {
        AirbnbResponse airbnbResponse = restTemplate.getForObject(testAirbnbUrl, AirbnbResponse.class);
        Assert.notNull(airbnbResponse);
    }

    @Test
    public void testResultsJsonWrapper() {
        AirbnbResponse airbnbResponse = restTemplate.getForObject(testAirbnbUrl, AirbnbResponse.class);
        Assert.notNull(airbnbResponse.getSearch_results());
    }

    @Test
    public void testListingWrapper() {
        AirbnbResponse airbnbResponse = restTemplate.getForObject(testAirbnbUrl, AirbnbResponse.class);
        Assert.notNull(airbnbResponse.getSearch_results().get(0));
    }

    @Test
    public void testPricingQuoteWrapper() {
        AirbnbResponse airbnbResponse = restTemplate.getForObject(testAirbnbUrl, AirbnbResponse.class);
        Assert.notNull(airbnbResponse.getSearch_results().get(0).getPricing_quote());
    }
}
